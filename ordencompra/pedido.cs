﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Softech.Administrativo.DefinicionBO;
using Softech.Administrativo.BusinessObjects;
using DevExpress.UserSkins;
using DevExpress.XtraEditors;
using DevExpress.Skins;
using System.IO;

namespace Requerimientos
{
    public partial class pedido : XtraForm
    {
        public static pedido f1;
        public Object ObjFormaUIPC = new Object();
        public Object ObjForma = new Object();
        public Object ObjGlobalFormaUIPC = new object();
        public Object ObjGlobalForma = new object();
        public DataTable dt = new DataTable();
        private string codigoProv = "";
        public string codigoLinea = "";
        public string codigoSubLinea = "";
        public string valores_permitidos = "0123456789";
        public string Letras = "abcdefghijklmnopqrstuvwxyz";
        
        // public string[] collection;
        public pedido(Object Forma, Object FormaIUPC,string codigoProveedor,string nombreProveedor)//,string co_lin,string co_subli)
        {
            InitializeComponent();
            ObjGlobalForma = Forma;
            ObjGlobalFormaUIPC = FormaIUPC;
      
            pedido.f1 = this;
            BonusSkins.Register();
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.Skins.SkinManager.EnableMdiFormSkins();
            //Register!
            //** 18/09/2020 **// 
         
            OfficeSkins.Register();
            DevExpress.Skins.SkinManager.Default.RegisterAssembly(typeof(OfficeSkins).Assembly);
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("Blue");
           
            txtCodigoProveedor.Text = codigoProveedor;
          
            txtNombreProveedor.Text = nombreProveedor;
           
            codigoProv = codigoProveedor;
            // MessageBox.Show(""+codigoProv);
            //linea, sublinea,proveedor
            // codigoLinea = co_lin;
            // codigoSubLinea = co_subli;
          
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.LightCyan;
            dataGridView1.RowsDefaultCellStyle.BackColor = Color.White;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.LightCyan;
            dataGridView1.RowHeadersDefaultCellStyle.BackColor = Color.LightCyan;
        }

        private void pedido_Load(object sender, EventArgs e)
        {


            // dt.Clear();


            /*
            StringBuilder consulta = new StringBuilder();
            consulta.Append("  select distinct a.co_art as codigo,a.art_des as descripcion, a.modelo as modelo,unidad.co_uni as unidad, " +
                "  a.stock_min as minimo, a.stock_max as maximo,a.stock_pedido as pedido, " +
                " stock.co_alma as almacen,precio.monto as codigoprecio, a.tipo_imp as impuesto, " +//precio.monto as codigoprecio,
                "  stock.stock as stock, 0 as nuevopedido " +
                " from saarticulo a  inner join  saArtProveedorReng p on a.co_art = p.co_art " +
                " inner join saArtPrecio precio on a.co_art = precio.co_art " +
                " inner join saartunidad unidad on a.co_art=unidad.co_art " +
                " inner join saStockAlmacen stock on a.co_art=stock.co_art " +
                " where p.co_prov = @co_prov " +
                " and a.co_lin = @co_lin and stock.co_alma='120001' and precio.co_precio ='01' ");//and a.co_subl=@co_subli ");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = consulta.ToString();
            cmd.Parameters.AddWithValue("@co_prov", codigoProv.Trim());
            cmd.Parameters.AddWithValue("@co_lin", codigoLinea.Trim());
           // cmd.Parameters.AddWithValue("@co_subli", codigoSubLinea.Trim());
            bool IsMaster = false;
            DataTable dtLlenarGrid = ((DataSet)ObjGlobalFormaUIPC.GetType().GetMethod("EjecutarComandoSql").Invoke(ObjGlobalFormaUIPC, new object[] { ((object)cmd), ((object)IsMaster) })).Tables[0];
            if (dtLlenarGrid.Rows.Count > 0)
            {
                dt = dtLlenarGrid;

            }


              dataGridView1.DataSource = dt;*/

            //  DataGridViewCheckBoxColumn chk = new DataGridViewCheckBoxColumn();
            // dataGridView1.Columns.Add(chk);
            dt.Columns.Add("Codigo");
            dt.Columns.Add("descripcion");
            dt.Columns.Add("modelo");
            dt.Columns.Add("unidad");
            dt.Columns.Add("minimo");
            dt.Columns.Add("maximo");
            dt.Columns.Add("pedido");
            dt.Columns.Add("almacen");
            dt.Columns.Add("almacenuno");
            dt.Columns.Add("almacendos");
            dt.Columns.Add("codigoprecio");
            dt.Columns.Add("impuesto");
            dt.Columns.Add("stock");
            dt.Columns.Add("nuevopedido");
            // dt.Columns.Add("unidadprincipal");
            // dt.Columns.Add("unidadsecundaria");
            string Codigo = "";
            string descripcion = "";
            string modelo = "";
            string unidad = "";
            decimal minimo = 0;
            decimal maximo = 0;
            decimal pedido = 0;
            string almacen = "";
            string almacen1 = "";
            string almacen2 = "";
            decimal codigoprecio = 0.0m;
            string impuesto = "";
            decimal stock = 0;
            int unidadprincipal;
            int unidadsecundaria;


            foreach (var a in filtro.f1.collection)
            {
                /*consulta original
                            consulta2.Append("  select distinct a.co_art as codigo,a.art_des as descripcion, a.modelo as modelo,unidad.co_uni as unidad, " +
                    "  a.stock_min as minimo, a.stock_max as maximo,a.stock_pedido as pedido, " +
                    " stock.co_alma as almacen,precio.monto as codigoprecio, a.tipo_imp as impuesto, " +//precio.monto as codigoprecio,
                    "  stock.stock as stock, 0 as nuevopedido " +
                    " from saarticulo a  inner join  saArtProveedorReng p on a.co_art = p.co_art " +
                    " inner join saArtPrecio precio on a.co_art = precio.co_art " +
                    " inner join saartunidad unidad on a.co_art=unidad.co_art " +
                    " inner join saStockAlmacen stock on a.co_art=stock.co_art " +
                    " where p.co_prov = @co_prov " +
                    " and a.co_lin = @co_lin and stock.co_alma='120001' and precio.co_precio ='01' ");//and a.co_subl=@co_subli ");*/
                // MessageBox.Show("1"+a.lineaBuscar);

                /* StringBuilder consulta2 = new StringBuilder();
                 consulta2.Append(" select distinct a.co_art as codigo,a.art_des as descripcion, a.modelo as modelo,unidad.co_uni as unidad, " +
                     "  a.stock_min as minimo, a.stock_max as maximo,a.stock_pedido as pedido,  stock.co_alma as almacen, " +
                      "  CASE "+
                     "  WHEN stock.co_alma in ('120001', '120002', '120003')  then stock.stock "+
                     " else '0' "+
                     "  END AS almacenuno, "+ 
                      " CASE " +
                     "  WHEN stock.co_alma between '200101' and '203901' then stock.stock "+
                     " else '0' "+
                     "   END AS almacendos,   " +
                     " precio.monto as codigoprecio, a.tipo_imp as impuesto, " +//precio.monto as codigoprecio,
                     "  stock.stock as stock, 0 as nuevopedido " +
                     " from saarticulo a  inner join  saArtProveedorReng p on a.co_art = p.co_art " +
                     " inner join saArtPrecio precio on a.co_art = precio.co_art " +
                     " inner join saartunidad unidad on a.co_art=unidad.co_art " +
                     " inner join saStockAlmacen stock on a.co_art=stock.co_art " +
                     " where p.co_prov = @co_prov " +
                     " and a.co_lin = @co_lin and (stock.co_alma in ('120001', '120002', '120003') or (stock.co_alma between '200101'and '203901')) "+
                     " and precio.co_precio ='01' "+
                     "  and unidad.uni_principal = '1'  ");//and a.co_subl=@co_subli ");*/
                StringBuilder consulta2 = new StringBuilder();
                /* consulta2.Append(" DECLARE @tablaProductos AS TABLE "+
                                     " (codigo varchar(20), descripcion varchar(100), modelo varchar(20), unidad varchar(10), "+
                                     " minimo decimal(18, 5), maximo decimal(18, 5), pedido decimal(18, 5), almacen varchar(20), " +
                                     " almacenuno decimal(18, 5), almacendos decimal(18, 5), codigoprecio decimal(18, 5), impuesto int, " +
                                     " stock decimal(18, 5), nuevopedido decimal(18, 5), unidadprincipal int, unidadsecundaria int,rowguid varchar(128)) " +
                                     " insert into @tablaProductos " +
                                     " (codigo, descripcion, modelo, unidad, " +
                                     " minimo, maximo, pedido, almacen, " +
                                     " almacenuno, almacendos, codigoprecio, impuesto, " +
                                     " stock, nuevopedido, unidadprincipal, unidadsecundaria,rowguid) " +
                                     " select distinct a.co_art as codigo, a.art_des as descripcion, a.modelo as modelo, unidad.co_uni as unidad, " +
                                     "     a.stock_min as minimo, a.stock_max as maximo, a.stock_pedido as pedido, stock.co_alma as almacen, " +
                                     "          CASE " +
                                     "    WHEN stock.co_alma in ('120001', '120002', '120003')  then stock.stock " +
                                     "    else '0' " +
                                     "     END AS almacenuno,  " +
                                     "     CASE " +
                                     "     WHEN stock.co_alma between '200101' and '203901' then stock.stock " +
                                     "    else '0' " +
                                     "     END AS almacendos, " +
                                     "   precio.monto as codigoprecio, a.tipo_imp as impuesto, " +
                                     "    stock.stock as stock, 0 as nuevopedido, " +
                                     "	 unidad.uni_principal as unidadprincipal, " +
                                     "	 unidad.uni_secundaria as unidadsecundaria, " +
                                     "  a.rowguid as rowguid "+
                                     "   from saarticulo a  inner " +
                                     "   join saArtProveedorReng p on a.co_art = p.co_art " +
                                     "    inner " +
                                     "   join saArtPrecio precio on a.co_art = precio.co_art " +
                                     "   inner " +
                                     "   join saartunidad unidad on a.co_art = unidad.co_art " +
                                     "   inner " +
                                     "   join saStockAlmacen stock on a.co_art = stock.co_art " +
                                     "    where p.co_prov = @co_prov " +
                                     "    and a.co_lin = @co_lin and(stock.co_alma in ('120001', '120002', '120003') or(stock.co_alma between '200101'and '203901'))  " +
                                     "    and precio.co_precio = '01' order by descripcion " +
                                     "  update @tablaProductos set stock = stock * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad), " +
                                     " almacenuno = almacenuno * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad), " +
                                     " almacendos = almacendos * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad), " +
                                     " pedido = pedido * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad),   " +
                                    " codigoprecio=isnull((select top(1)isnull(costo,0) from saCostoHistoricoEntrada where cod_articulo_rowguid = rowguid and cod_almacen = almacen and tipo_doc = 'prov' order by fecha_registro desc),0)    " +
                                    "  update @tablaProductos set codigoprecio = codigoprecio / (select equivalencia from saartunidad uni where uni.co_art=codigo and uni.co_uni=unidad)  " +
                                    " select* from @tablaProductos where unidadsecundaria = 1 ");*/
                consulta2.Append(" exec nssp_buscarproductos @co_prov,@co_lin,'0','1','search' ");
                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = consulta2.ToString();
                cmd2.Parameters.AddWithValue("@co_prov", codigoProv.Trim());
                    cmd2.Parameters.AddWithValue("@co_lin", a.lineaBuscar.Trim()); //"cmed");// codigoLinea.Trim());
                // cmd.Parameters.AddWithValue("@co_subli", codigoSubLinea.Trim());
                bool IsMaster2 = false;
                DataTable dtLlenarGrid2 = ((DataSet)ObjGlobalFormaUIPC.GetType().GetMethod("EjecutarComandoSql").Invoke(ObjGlobalFormaUIPC, new object[] { ((object)cmd2), ((object)IsMaster2) })).Tables[0];
                if (dtLlenarGrid2.Rows.Count > 0)
                {

                      //  string recibido = "";
                    foreach (DataRow item in dtLlenarGrid2.Rows)
                    {
                        Codigo = Convert.ToString(item["codigo"].ToString().Trim());
                        descripcion = Convert.ToString(item["descripcion"]);
                        modelo = Convert.ToString(item["modelo"]);
                        unidad = Convert.ToString(item["unidad"]);  
                        minimo = Convert.ToDecimal(item["minimo"]);
                        maximo = Convert.ToDecimal(item["maximo"]);
                        pedido = Convert.ToDecimal(item["pedido"]);
                        almacen = Convert.ToString(item["almacen"]);
                        almacen1 = Convert.ToString(item["almacenuno"]);
                        almacen2 = Convert.ToString(item["almacendos"]);
                        // recibido = string.Format("{0:0.00}", item["codigoprecio"]);
                        codigoprecio = Convert.ToDecimal(item["codigoprecio"]);
                        impuesto = Convert.ToString(item["impuesto"]);
                        stock = Convert.ToDecimal(item["stock"]);
                        //unidadprincipal = Convert.ToInt32(item["unidadprincipal"]);
                        //  unidadsecundaria = Convert.ToInt32(item["unidadsecundaria"]);

                        dt.Rows.Add(Codigo, descripcion, modelo, unidad, minimo, maximo, pedido, almacen, almacen1,
                            almacen2, codigoprecio, impuesto, stock, 0);//, unidadprincipal, unidadsecundaria);


                    }

                }
                }

            dataGridView1.DataSource = dt;

              foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (((Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockminimo"].Value)) * 1.10) >= Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value))
                    {

                        row.Cells["nuevopedido"].Value = Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockmaximo"].Value) - Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value);

                    }
                if (((Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockminimo"].Value)) * 1.10) >= Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value))
                {
                    row.Cells["stockactual"].Style.BackColor = Color.Red;
                    //row.DefaultCellStyle.BackColor = Color.Red;
                    //row.DefaultCellStyle.ForeColor = Color.Red;
                    // row.Cells["nuevopedido"].Value = Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockmaximo"].Value) - Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value);
                    // dataGridView1.Rows[10].DefaultCellStyle.BackColor = Color.Red;
                }
                if (((Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockmaximo"].Value)) * 0.9) <= Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value))
                {
                    row.Cells["stockactual"].Style.BackColor = Color.Green;
                    //row.DefaultCellStyle.BackColor = Color.Red;
                    //row.DefaultCellStyle.ForeColor = Color.Red;

                    // dataGridView1.Rows[10].DefaultCellStyle.BackColor = Color.Red;
                }
            }

            }

            private void btnSalir_Click(object sender, EventArgs e)
            {
                this.Close();
            }

            private void btnAceptar_Click(object sender, EventArgs e)




            {
                /* foreach (DataGridViewRow row in dataGridView1.Rows)
                 {
                     if (Convert.ToBoolean(row.Cells["seleccion"].Value) == true)
                     {
                         MessageBox.Show(string.Format(" el codigo del articulo es {0} y el nombre es {1} ",
                             Convert.ToString(row.Cells["codigo"].Value),Convert.ToString(row.Cells["Descripcion_articulo"].Value)));
                     }
                 }*/

                List<IOrdenCompraRenglonBO> listaOrdenCompra;
            listaOrdenCompra = (List<IOrdenCompraRenglonBO>)ObjGlobalFormaUIPC.GetType().GetMethod("ObtenerObjetoRenglonesReq").Invoke(ObjGlobalFormaUIPC, new Object[] { });
            if (listaOrdenCompra.Count > 0)
            {
                try
                {

                    IOrdenCompraBO encabezado2;
                    encabezado2 = (Softech.Administrativo.DefinicionBO.IOrdenCompraBO)ObjGlobalFormaUIPC.GetType().GetMethod("ObtenerObjetoReq").Invoke(ObjGlobalFormaUIPC, new Object[] { });

                    foreach (DataGridViewRow drcotiz in dataGridView1.Rows)
                    {
                        if (Convert.ToBoolean(drcotiz.Cells["seleccion"].Value) == true)
                        {

                       
                        bool esprimero2 = (String.IsNullOrEmpty(encabezado2.Renglones.Lista[0].Co_Art));
                        Softech.Administrativo.BusinessObjects.OrdenCompraRenglonBO Reng;

                        if (esprimero2)
                            Reng = (Softech.Administrativo.BusinessObjects.OrdenCompraRenglonBO)encabezado2.Renglones.Lista[0];

                        else
                            Reng = new Softech.Administrativo.BusinessObjects.OrdenCompraRenglonBO();

                        if (!esprimero2)
                            encabezado2.AgregarListaRenglones(encabezado2.Renglones.Lista.Count(), Reng);
                         
                            int verificar = Convert.ToInt32(drcotiz.Cells["TipoImpuesto"].Value);
                         
                            Reng.Co_Art = Convert.ToString(drcotiz.Cells["codigo"].Value);
                            
                            Reng.Co_Uni = (String)drcotiz.Cells["unidad"].Value;
                           
                            //  Reng.Modelo = (string)drcotiz.Cells["modelo"].Value;
                            // Reng.Co_Alma = ((string)drcotiz.Cells["almacen"].Value)??"CCS";
                            Reng.Co_Alma = drcotiz.Cells["almacen"].Value ==DBNull.Value?"CCS" : (string)drcotiz.Cells["almacen"].Value;
                           
                            // MessageBox.Show(""+Convert.ToChar(verificar).ToString());
                            //  Reng.Tipo_Imp =//verificar;// Convert.ToString(verificar);
                            Reng.Cost_Unit = Convert.ToDecimal(drcotiz.Cells["costo"].Value);
                         
                            Reng.Art_Des = (String)drcotiz.Cells["Descripcion_articulo"].Value;
                         
                            Reng.Total_Art = Convert.ToInt32(drcotiz.Cells["nuevopedido"].Value);
                            // (int)drcotiz.Cells["TipoImpuesto"].Value;
                             StringBuilder consultacb = new StringBuilder();
                              consultacb.Append(" select top(1)porc_tasa as porc,tipo_imp as tipo from saImpuestoSobreVentaReng  where tipo_imp = @tipoimp  order by fecha desc ");
                            
                            SqlCommand cmdcb = new SqlCommand();
                              cmdcb.CommandType = CommandType.Text;
                              cmdcb.CommandText = consultacb.ToString();
                              cmdcb.Parameters.AddWithValue("@tipoimp", verificar );
                              bool IsMastercb = false;
                              DataTable dt3 = ((DataSet)ObjGlobalFormaUIPC.GetType().GetMethod("EjecutarComandoSql").Invoke(ObjGlobalFormaUIPC, new object[] { ((object)cmdcb), ((object)IsMastercb) })).Tables[0];
                              foreach (DataRow item in dt3.Rows)
                              {
                               
                                Reng.Tipo_Imp = Convert.ToChar(item["tipo"]);
                              
                                Reng.Porc_Imp = Convert.ToDecimal(item["porc"]);
                              }

                            //Reng.Porc_Imp = (decimal)dt3.Rows[0]["porc"];
                  

                        }
                       // ObjGlobalFormaUIPC.GetType().GetMethod("ActualizarGrid").Invoke(ObjGlobalFormaUIPC, new object[] { });
                    }
                }
                catch (Exception exe)
                {

                    MessageBox.Show("Error:::" + exe, "Nikko Apps", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }

            /*
            INotaRecepcionBO encabezado;
            encabezado = (Softech.Administrativo.DefinicionBO.INotaRecepcionBO)
                ObjFormaUIPC.GetType().GetMethod("ObtenerObjetoReq").Invoke(ObjFormaUIPC, new Object[] { });

            foreach (DataRow drcotiz in dt2.Rows)
            {
                bool esprimero = (String.IsNullOrEmpty(encabezado.Renglones.Lista[0].Co_Art));
                Softech.Administrativo.BusinessObjects.NotaRecepcionRenglonBO pedReng;

                if (esprimero)
                    pedReng = (Softech.Administrativo.BusinessObjects.NotaRecepcionRenglonBO)encabezado.Renglones.Lista[0];

                else
                    pedReng = new Softech.Administrativo.BusinessObjects.NotaRecepcionRenglonBO();
                pedReng.Pk_Doc_Num = Convert.ToString(encabezado.Renglones.Lista.Count());
      
                pedReng.Reng_Doc = (int)drcotiz["reng_num"];//campos del grid
                pedReng.Co_Art = (String)drcotiz["Co_Art"];
                pedReng.Total_Art = Convert.ToDecimal(textBox22.Text);//(Decimal)drcotiz["Total_Art"];

                //pedReng.Modelo = (String)drcotiz["modelo"];
                pedReng.Art_Des = (String)drcotiz["art_des"];
                pedReng.Co_Alma = (String)drcotiz["co_alma"];
                pedReng.Cost_Unit = (Decimal)drcotiz["cost_unit"];
                pedReng.Tipo_Imp = Convert.ToChar(drcotiz["tipo_imp"]);
                pedReng.Porc_Imp = (Decimal)drcotiz["porc_imp"];
                pedReng.Co_Uni = (string)drcotiz["co_uni"];
                pedReng.Num_Doc = (string)drcotiz["doc_num"];
                pedReng.Tipo_Doc = "OCOM";


                pedReng.Rowguid_Doc = (Guid)drcotiz["rowguid"];

                if (listarengrecepcion.Count > 1)
                {
                    encabezado.EliminarListaRenglones(encabezado.Renglones.Lista[0]);
                }

                ObjFormaUIPC.GetType().GetMethod("ActualizarGrid").Invoke(ObjFormaUIPC, new object[] { });
                DateTime uno = DateTime.Now;
                notarecepcion.F1.nopescac = textBox1.Text;
                notarecepcion.F1.nopesdes = textBox2.Text;
                notarecepcion.F1.fecpescac = dateTimePicker1.Value;
                notarecepcion.F1.fecpesdes = dateTimePicker2.Value;
                notarecepcion.F1.tr = comboBox1.Text;
                notarecepcion.F1.odc = textBox7.Text;
      
                notarecepcion.F1.fzacasa = textBox10.Text;
                notarecepcion.F1.fzades = textBox11.Text;
                notarecepcion.F1.flemacac = textBox12.Text;
                notarecepcion.F1.flemades = textBox13.Text;
      
                notarecepcion.F1.faaCacsa = textBox15.Text;
                notarecepcion.F1.fpCacsa = textBox17.Text;
                notarecepcion.F1.faaDestileria = textBox16.Text;
                notarecepcion.F1.fpDestileria = textBox18.Text;
                //nuevas 07072020
                notarecepcion.F1.ciChofer = comboBox2.Text;
                notarecepcion.F1.nombreChofer = textBox5.Text;
                notarecepcion.F1.placaCisterna = textBox6.Text;
                notarecepcion.F1.renglon = comboBox4.Text;
                //hasta aqui las nuevas 07072020
                notarecepcion.F1.tipo = "Recepcion de Flema";
            
                this.Dispose();
                */

            // }
            this.Close();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
           /*  if (dataGridView1.Columns[e.ColumnIndex].Name == "stockminimo")  //Si es la columna a evaluar
             {
                 if (Convert.ToDouble(e.Value.ToString())>=Convert.ToDouble(dataGridView1.Rows[10].ToString()))   //Si el valor de la celda contiene la palabra hora
                 {
                     dataGridView1.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                    // dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
                     // e.CellStyle.BackColor = Color.Red;
                     // e.CellStyle.ForeColor = Color.Red;
                 }
             }*/
        /*  foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (((Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockminimo"].Value))*1.10)>= Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value))
                {
                    row.Cells["stockactual"].Style.BackColor = Color.Red;
                    //row.DefaultCellStyle.BackColor = Color.Red;
                    //row.DefaultCellStyle.ForeColor = Color.Red;
                   // row.Cells["nuevopedido"].Value = Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockmaximo"].Value) - Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value);
                   // dataGridView1.Rows[10].DefaultCellStyle.BackColor = Color.Red;
                }
                if (((Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockmaximo"].Value))*0.9) <= Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value))
                {
                    row.Cells["stockactual"].Style.BackColor = Color.Green;
                    //row.DefaultCellStyle.BackColor = Color.Red;
                    //row.DefaultCellStyle.ForeColor = Color.Red;

                    // dataGridView1.Rows[10].DefaultCellStyle.BackColor = Color.Red;
                }
            }*/
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox txt = e.Control as TextBox;
            txt.KeyPress -= new KeyPressEventHandler(validaTecla);
            txt.KeyPress += new KeyPressEventHandler(validaTecla);
        }

        private void validaTecla(object sender, KeyPressEventArgs e)
        {

            string FormatoCelda = this.dataGridView1.Columns[this.dataGridView1.CurrentCell.ColumnIndex].DefaultCellStyle.Format.ToString();
            if (e.KeyChar == 46)
            {
                e.KeyChar = ',';
            }
            if (FormatoCelda == "")
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
                e.Handled = !(Letras.ToUpper() + Letras + "\b").Contains(e.KeyChar);//se puede colocar numeros y mas signos"-@\b"
            }
            else
            {
                if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == '\b')
                {
                    e.Handled = false;
                }
                else if (e.KeyChar == ',')
                {

                    e.Handled = ((System.Windows.Forms.TextBox)sender).Text.Contains(",");

                }
                else
                {
                    e.Handled = true;
                }
            }
            // TextBox txt = sender as TextBox;
            // string[] _decimal = txt.Text.Split(',');



        }

        private void btnPrimaria_Click(object sender, EventArgs e)
        {
            dt.Clear();
            foreach (var a in filtro.f1.collection)
            {
                StringBuilder consulta2 = new StringBuilder();
                /*     consulta2.Append(" DECLARE @tablaProductos AS TABLE " +
                                         " (codigo varchar(20), descripcion varchar(100), modelo varchar(20), unidad varchar(10), " +
                                         " minimo decimal(18, 5), maximo decimal(18, 5), pedido decimal(18, 5), almacen varchar(20), " +
                                         " almacenuno decimal(18, 5), almacendos decimal(18, 5), codigoprecio decimal(18, 5), impuesto int, " +
                                         " stock decimal(18, 5), nuevopedido decimal(18, 5), unidadprincipal int, unidadsecundaria int,rowguid varchar(128)) " +
                                         " insert into @tablaProductos " +
                                         " (codigo, descripcion, modelo, unidad, " +
                                         " minimo, maximo, pedido, almacen, " +
                                         " almacenuno, almacendos, codigoprecio, impuesto, " +
                                         " stock, nuevopedido, unidadprincipal, unidadsecundaria,rowguid) " +
                                         " select distinct a.co_art as codigo, a.art_des as descripcion, a.modelo as modelo, unidad.co_uni as unidad, " +
                                         "     a.stock_min as minimo, a.stock_max as maximo, a.stock_pedido as pedido, stock.co_alma as almacen, " +
                                         "          CASE " +
                                         "    WHEN stock.co_alma in ('120001', '120002', '120003')  then stock.stock " +
                                         "    else '0' " +
                                         "     END AS almacenuno,  " +
                                         "     CASE " +
                                         "     WHEN stock.co_alma between '200101' and '203901' then stock.stock " +
                                         "    else '0' " +
                                         "     END AS almacendos, " +
                                         "   precio.monto as codigoprecio, a.tipo_imp as impuesto, " +
                                         "    stock.stock as stock, 0 as nuevopedido, " +
                                         "	 unidad.uni_principal as unidadprincipal, " +
                                         "	 unidad.uni_secundaria as unidadsecundaria, " +
                                         "  a.rowguid as rowguid " +
                                         "   from saarticulo a  inner " +
                                         "   join saArtProveedorReng p on a.co_art = p.co_art " +
                                         "    inner " +
                                         "   join saArtPrecio precio on a.co_art = precio.co_art " +
                                         "   inner " +
                                         "   join saartunidad unidad on a.co_art = unidad.co_art " +
                                         "   inner " +
                                         "   join saStockAlmacen stock on a.co_art = stock.co_art " +
                                          "    where p.co_prov = @co_prov " +
                                         "    and a.co_lin = @co_lin  and(stock.co_alma in ('120001', '120002', '120003') or(stock.co_alma between '200101'and '203901'))  " +
                                         "    and precio.co_precio = '01' order by descripcion " +
                                         "  update @tablaProductos set stock = stock * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad), " +
                                         " almacenuno = almacenuno * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad), " +
                                         " almacendos = almacendos * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad), " +
                                         " pedido = pedido * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad),   " +
                                        " codigoprecio=isnull((select top(1)isnull(costo,0) from saCostoHistoricoEntrada where cod_articulo_rowguid = rowguid and cod_almacen = almacen and tipo_doc = 'prov' order by fecha_registro desc),0)    " +
                                        "  update @tablaProductos set codigoprecio = codigoprecio / (select equivalencia from saartunidad uni where uni.co_art=codigo and uni.co_uni=unidad)  " +
                                        " select* from @tablaProductos where unidadprincipal = 1 ");
                                        */
                consulta2.Append(" exec nssp_buscarproductos @co_prov,@co_lin,'1','0','search' ");
                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = consulta2.ToString();
                cmd2.Parameters.AddWithValue("@co_prov", codigoProv.Trim());
                cmd2.Parameters.AddWithValue("@co_lin", a.lineaBuscar.Trim()); //"cmed");// codigoLinea.Trim());
                                                                               // cmd.Parameters.AddWithValue("@co_subli", codigoSubLinea.Trim());
                bool IsMaster2 = false;
                DataTable dtLlenarGrid2 = ((DataSet)ObjGlobalFormaUIPC.GetType().GetMethod("EjecutarComandoSql").Invoke(ObjGlobalFormaUIPC, new object[] { ((object)cmd2), ((object)IsMaster2) })).Tables[0];
                if (dtLlenarGrid2.Rows.Count > 0)
                {
                    string Codigo = "";
                    string descripcion = "";
                    string modelo = "";
                    string unidad = "";
                    decimal minimo = 0;
                    decimal maximo = 0;
                    decimal pedido = 0;
                    string almacen = "";
                    string almacen1 = "";
                    string almacen2 = "";
                    decimal codigoprecio = 0.0m;
                    string impuesto = "";
                    decimal stock = 0;
                    int unidadprincipal;
                    int unidadsecundaria;
                    //  string recibido = "";
                    foreach (DataRow item in dtLlenarGrid2.Rows)
                    {
                        Codigo = Convert.ToString(item["codigo"].ToString().Trim());
                        descripcion = Convert.ToString(item["descripcion"]);
                        modelo = Convert.ToString(item["modelo"]);
                        unidad = Convert.ToString(item["unidad"]);
                        minimo = Convert.ToDecimal(item["minimo"]);
                        maximo = Convert.ToDecimal(item["maximo"]);
                        pedido = Convert.ToDecimal(item["pedido"]);
                        almacen = Convert.ToString(item["almacen"]);
                        almacen1 = Convert.ToString(item["almacenuno"]);
                        almacen2 = Convert.ToString(item["almacendos"]);
                        // recibido = string.Format("{0:0.00}", item["codigoprecio"]);
                        codigoprecio = Convert.ToDecimal(item["codigoprecio"]);
                        impuesto = Convert.ToString(item["impuesto"]);
                        stock = Convert.ToDecimal(item["stock"]);
                        //unidadprincipal = Convert.ToInt32(item["unidadprincipal"]);
                        //  unidadsecundaria = Convert.ToInt32(item["unidadsecundaria"]);

                        dt.Rows.Add(Codigo, descripcion, modelo, unidad, minimo, maximo, pedido, almacen, almacen1,
                            almacen2, codigoprecio, impuesto, stock, 0);//, unidadprincipal, unidadsecundaria);


                    }

                }

            }
                    dataGridView1.DataSource = dt;

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (((Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockminimo"].Value)) * 1.10) >= Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value))
                    {

                        row.Cells["nuevopedido"].Value = Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockmaximo"].Value) - Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value);

                    }

                if (((Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockminimo"].Value)) * 1.10) >= Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value))
                {
                    row.Cells["stockactual"].Style.BackColor = Color.Red;
                    //row.DefaultCellStyle.BackColor = Color.Red;
                    //row.DefaultCellStyle.ForeColor = Color.Red;
                    // row.Cells["nuevopedido"].Value = Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockmaximo"].Value) - Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value);
                    // dataGridView1.Rows[10].DefaultCellStyle.BackColor = Color.Red;
                }
                if (((Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockmaximo"].Value)) * 0.9) <= Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value))
                {
                    row.Cells["stockactual"].Style.BackColor = Color.Green;
                    //row.DefaultCellStyle.BackColor = Color.Red;
                    //row.DefaultCellStyle.ForeColor = Color.Red;

                    // dataGridView1.Rows[10].DefaultCellStyle.BackColor = Color.Red;
                }
            }
        }

        private void btnSecundaria_Click(object sender, EventArgs e)
        {
            dt.Clear();
            foreach (var a in filtro.f1.collection)
            {
                StringBuilder consulta2 = new StringBuilder();
                /*  consulta2.Append(" DECLARE @tablaProductos AS TABLE " +
                                      " (codigo varchar(20), descripcion varchar(100), modelo varchar(20), unidad varchar(10), " +
                                      " minimo decimal(18, 5), maximo decimal(18, 5), pedido decimal(18, 5), almacen varchar(20), " +
                                      " almacenuno decimal(18, 5), almacendos decimal(18, 5), codigoprecio decimal(18, 5), impuesto int, " +
                                      " stock decimal(18, 5), nuevopedido decimal(18, 5), unidadprincipal int, unidadsecundaria int,rowguid varchar(128)) " +
                                      " insert into @tablaProductos " +
                                      " (codigo, descripcion, modelo, unidad, " +
                                      " minimo, maximo, pedido, almacen, " +
                                      " almacenuno, almacendos, codigoprecio, impuesto, " +
                                      " stock, nuevopedido, unidadprincipal, unidadsecundaria,rowguid) " +
                                      " select distinct a.co_art as codigo, a.art_des as descripcion, a.modelo as modelo, unidad.co_uni as unidad, " +
                                      "     a.stock_min as minimo, a.stock_max as maximo, a.stock_pedido as pedido, stock.co_alma as almacen, " +
                                      "          CASE " +
                                      "    WHEN stock.co_alma in ('120001', '120002', '120003')  then stock.stock " +
                                      "    else '0' " +
                                      "     END AS almacenuno,  " +
                                      "     CASE " +
                                      "     WHEN stock.co_alma between '200101' and '203901' then stock.stock " +
                                      "    else '0' " +
                                      "     END AS almacendos, " +
                                      "   precio.monto as codigoprecio, a.tipo_imp as impuesto, " +
                                      "    stock.stock as stock, 0 as nuevopedido, " +
                                      "	 unidad.uni_principal as unidadprincipal, " +
                                      "	 unidad.uni_secundaria as unidadsecundaria, " +
                                      "  a.rowguid as rowguid " +
                                      "   from saarticulo a  inner " +
                                      "   join saArtProveedorReng p on a.co_art = p.co_art " +
                                      "    inner " +
                                      "   join saArtPrecio precio on a.co_art = precio.co_art " +
                                      "   inner " +
                                      "   join saartunidad unidad on a.co_art = unidad.co_art " +
                                      "   inner " +
                                      "   join saStockAlmacen stock on a.co_art = stock.co_art " +
                                      "    where p.co_prov = @co_prov " +
                                      "    and a.co_lin = @co_lin and(stock.co_alma in ('120001', '120002', '120003') or(stock.co_alma between '200101'and '203901'))  " +
                                      "    and precio.co_precio = '01' order by descripcion " +
                                      "  update @tablaProductos set stock = stock * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad), " +
                                      " almacenuno = almacenuno * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad), " +
                                      " almacendos = almacendos * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad), " +
                                      " pedido = pedido * (select equivalencia from saartunidad uni where uni.co_art = codigo and uni.co_uni = unidad),   " +
                                      " codigoprecio=isnull((select top(1)isnull(costo,0) from saCostoHistoricoEntrada where cod_articulo_rowguid = rowguid and cod_almacen = almacen and tipo_doc = 'prov' order by fecha_registro desc),0)    " +
                                     "  update @tablaProductos set codigoprecio = codigoprecio / (select equivalencia from saartunidad uni where uni.co_art=codigo and uni.co_uni=unidad)  " +
                                     " select* from @tablaProductos where unidadsecundaria = 1 ");
                                     */
                consulta2.Append(" exec nssp_buscarproductos @co_prov,@co_lin,'0','1','search' ");
                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = consulta2.ToString();
                cmd2.Parameters.AddWithValue("@co_prov", codigoProv.Trim());
                cmd2.Parameters.AddWithValue("@co_lin", a.lineaBuscar.Trim()); //"cmed");// codigoLinea.Trim());
                                                                               // cmd.Parameters.AddWithValue("@co_subli", codigoSubLinea.Trim());
                bool IsMaster2 = false;
                DataTable dtLlenarGrid2 = ((DataSet)ObjGlobalFormaUIPC.GetType().GetMethod("EjecutarComandoSql").Invoke(ObjGlobalFormaUIPC, new object[] { ((object)cmd2), ((object)IsMaster2) })).Tables[0];
                if (dtLlenarGrid2.Rows.Count > 0)
                {
                    string Codigo = "";
                    string descripcion = "";
                    string modelo = "";
                    string unidad = "";
                    decimal minimo = 0;
                    decimal maximo = 0;
                    decimal pedido = 0;
                    string almacen = "";
                    string almacen1 = "";
                    string almacen2 = "";
                    decimal codigoprecio = 0.0m;
                    string impuesto = "";
                    decimal stock = 0;
                    int unidadprincipal;
                    int unidadsecundaria;
                    //  string recibido = "";
                    foreach (DataRow item in dtLlenarGrid2.Rows)
                    {
                        Codigo = Convert.ToString(item["codigo"].ToString().Trim());
                        descripcion = Convert.ToString(item["descripcion"]);
                        modelo = Convert.ToString(item["modelo"]);
                        unidad = Convert.ToString(item["unidad"]);
                        minimo = Convert.ToDecimal(item["minimo"]);
                        maximo = Convert.ToDecimal(item["maximo"]);
                        pedido = Convert.ToDecimal(item["pedido"]);
                        almacen = Convert.ToString(item["almacen"]);
                        almacen1 = Convert.ToString(item["almacenuno"]);
                        almacen2 = Convert.ToString(item["almacendos"]);
                        // recibido = string.Format("{0:0.00}", item["codigoprecio"]);
                        codigoprecio = Convert.ToDecimal(item["codigoprecio"]);
                        impuesto = Convert.ToString(item["impuesto"]);
                        stock = Convert.ToDecimal(item["stock"]);
                        //unidadprincipal = Convert.ToInt32(item["unidadprincipal"]);
                        //  unidadsecundaria = Convert.ToInt32(item["unidadsecundaria"]);

                        dt.Rows.Add(Codigo, descripcion, modelo, unidad, minimo, maximo, pedido, almacen, almacen1,
                            almacen2, codigoprecio, impuesto, stock, 0);//, unidadprincipal, unidadsecundaria);


                    }

                }
            }

            dataGridView1.DataSource = dt;

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (((Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockminimo"].Value)) * 1.10) >= Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value))
                {

                    row.Cells["nuevopedido"].Value = Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockmaximo"].Value) - Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value);

                }

                if (((Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockminimo"].Value)) * 1.10) >= Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value))
                {
                    row.Cells["stockactual"].Style.BackColor = Color.Red;
                    //row.DefaultCellStyle.BackColor = Color.Red;
                    //row.DefaultCellStyle.ForeColor = Color.Red;
                    // row.Cells["nuevopedido"].Value = Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockmaximo"].Value) - Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value);
                    // dataGridView1.Rows[10].DefaultCellStyle.BackColor = Color.Red;
                }
                if (((Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockmaximo"].Value)) * 0.9) <= Convert.ToDouble(dataGridView1.Rows[row.Index].Cells["stockactual"].Value))
                {
                    row.Cells["stockactual"].Style.BackColor = Color.Green;
                    //row.DefaultCellStyle.BackColor = Color.Red;
                    //row.DefaultCellStyle.ForeColor = Color.Red;

                    // dataGridView1.Rows[10].DefaultCellStyle.BackColor = Color.Red;
                }
            }
        }

        private void panelSuperior_Paint(object sender, PaintEventArgs e)
        {
            
            


        }
    }
}
