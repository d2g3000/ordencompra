﻿namespace Requerimientos
{
    partial class pedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(pedido));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelPrincipal = new System.Windows.Forms.Panel();
            this.panelCentral = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panelInferior = new System.Windows.Forms.Panel();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.panelSuperior = new System.Windows.Forms.Panel();
            this.btnSecundaria = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrimaria = new DevExpress.XtraEditors.SimpleButton();
            this.txtNombreProveedor = new System.Windows.Forms.TextBox();
            this.txtCodigoProveedor = new System.Windows.Forms.TextBox();
            this.lblNombreProveedor = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.lblUnidad = new System.Windows.Forms.Label();
            this.Seleccion = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion_articulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.almacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.almacenuno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.almacen2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockminimo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockmaximo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockpedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockactual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoImpuesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nuevopedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelPrincipal.SuspendLayout();
            this.panelCentral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panelInferior.SuspendLayout();
            this.panelSuperior.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelPrincipal
            // 
            this.panelPrincipal.Controls.Add(this.panelCentral);
            this.panelPrincipal.Controls.Add(this.panelInferior);
            this.panelPrincipal.Controls.Add(this.panelSuperior);
            this.panelPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPrincipal.Location = new System.Drawing.Point(0, 0);
            this.panelPrincipal.Name = "panelPrincipal";
            this.panelPrincipal.Size = new System.Drawing.Size(1165, 556);
            this.panelPrincipal.TabIndex = 0;
            // 
            // panelCentral
            // 
            this.panelCentral.Controls.Add(this.dataGridView1);
            this.panelCentral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCentral.Location = new System.Drawing.Point(0, 80);
            this.panelCentral.Name = "panelCentral";
            this.panelCentral.Size = new System.Drawing.Size(1165, 421);
            this.panelCentral.TabIndex = 2;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccion,
            this.Codigo,
            this.Descripcion_articulo,
            this.unidad,
            this.modelo,
            this.almacen,
            this.almacenuno,
            this.almacen2,
            this.costo,
            this.stockminimo,
            this.stockmaximo,
            this.stockpedido,
            this.stockactual,
            this.TipoImpuesto,
            this.nuevopedido});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1165, 421);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            // 
            // panelInferior
            // 
            this.panelInferior.BackColor = System.Drawing.Color.Transparent;
            this.panelInferior.Controls.Add(this.btnAceptar);
            this.panelInferior.Controls.Add(this.btnSalir);
            this.panelInferior.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelInferior.Location = new System.Drawing.Point(0, 501);
            this.panelInferior.Name = "panelInferior";
            this.panelInferior.Size = new System.Drawing.Size(1165, 55);
            this.panelInferior.TabIndex = 1;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Image = ((System.Drawing.Image)(resources.GetObject("btnAceptar.Image")));
            this.btnAceptar.Location = new System.Drawing.Point(929, 13);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(80, 30);
            this.btnAceptar.TabIndex = 1;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(1046, 13);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(80, 30);
            this.btnSalir.TabIndex = 0;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // panelSuperior
            // 
            this.panelSuperior.BackColor = System.Drawing.Color.Transparent;
            this.panelSuperior.Controls.Add(this.lblUnidad);
            this.panelSuperior.Controls.Add(this.btnSecundaria);
            this.panelSuperior.Controls.Add(this.btnPrimaria);
            this.panelSuperior.Controls.Add(this.txtNombreProveedor);
            this.panelSuperior.Controls.Add(this.txtCodigoProveedor);
            this.panelSuperior.Controls.Add(this.lblNombreProveedor);
            this.panelSuperior.Controls.Add(this.lblCodigo);
            this.panelSuperior.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSuperior.Location = new System.Drawing.Point(0, 0);
            this.panelSuperior.Name = "panelSuperior";
            this.panelSuperior.Size = new System.Drawing.Size(1165, 80);
            this.panelSuperior.TabIndex = 0;
            this.panelSuperior.Paint += new System.Windows.Forms.PaintEventHandler(this.panelSuperior_Paint);
            // 
            // btnSecundaria
            // 
            this.btnSecundaria.Location = new System.Drawing.Point(528, 40);
            this.btnSecundaria.Name = "btnSecundaria";
            this.btnSecundaria.Size = new System.Drawing.Size(75, 23);
            this.btnSecundaria.TabIndex = 5;
            this.btnSecundaria.Text = "Secundaria";
            this.btnSecundaria.Click += new System.EventHandler(this.btnSecundaria_Click);
            // 
            // btnPrimaria
            // 
            this.btnPrimaria.Location = new System.Drawing.Point(528, 10);
            this.btnPrimaria.Name = "btnPrimaria";
            this.btnPrimaria.Size = new System.Drawing.Size(75, 23);
            this.btnPrimaria.TabIndex = 4;
            this.btnPrimaria.Text = "Primaria";
            this.btnPrimaria.Click += new System.EventHandler(this.btnPrimaria_Click);
            // 
            // txtNombreProveedor
            // 
            this.txtNombreProveedor.Location = new System.Drawing.Point(161, 40);
            this.txtNombreProveedor.Name = "txtNombreProveedor";
            this.txtNombreProveedor.ReadOnly = true;
            this.txtNombreProveedor.Size = new System.Drawing.Size(220, 21);
            this.txtNombreProveedor.TabIndex = 3;
            // 
            // txtCodigoProveedor
            // 
            this.txtCodigoProveedor.Location = new System.Drawing.Point(75, 10);
            this.txtCodigoProveedor.Name = "txtCodigoProveedor";
            this.txtCodigoProveedor.ReadOnly = true;
            this.txtCodigoProveedor.Size = new System.Drawing.Size(100, 21);
            this.txtCodigoProveedor.TabIndex = 2;
            // 
            // lblNombreProveedor
            // 
            this.lblNombreProveedor.AutoSize = true;
            this.lblNombreProveedor.Location = new System.Drawing.Point(10, 43);
            this.lblNombreProveedor.Name = "lblNombreProveedor";
            this.lblNombreProveedor.Size = new System.Drawing.Size(121, 13);
            this.lblNombreProveedor.TabIndex = 1;
            this.lblNombreProveedor.Text = "Descripción Proveedor: ";
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Location = new System.Drawing.Point(10, 13);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(44, 13);
            this.lblCodigo.TabIndex = 0;
            this.lblCodigo.Text = "Código:";
            // 
            // lblUnidad
            // 
            this.lblUnidad.AutoSize = true;
            this.lblUnidad.Location = new System.Drawing.Point(482, 33);
            this.lblUnidad.Name = "lblUnidad";
            this.lblUnidad.Size = new System.Drawing.Size(40, 13);
            this.lblUnidad.TabIndex = 6;
            this.lblUnidad.Text = "Unidad";
            // 
            // Seleccion
            // 
            this.Seleccion.HeaderText = "Sel";
            this.Seleccion.Name = "Seleccion";
            this.Seleccion.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Seleccion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Seleccion.Width = 40;
            // 
            // Codigo
            // 
            this.Codigo.DataPropertyName = "codigo";
            this.Codigo.HeaderText = "Código";
            this.Codigo.Name = "Codigo";
            this.Codigo.ReadOnly = true;
            this.Codigo.Width = 80;
            // 
            // Descripcion_articulo
            // 
            this.Descripcion_articulo.DataPropertyName = "descripcion";
            this.Descripcion_articulo.HeaderText = "Descripción Artículo";
            this.Descripcion_articulo.Name = "Descripcion_articulo";
            this.Descripcion_articulo.ReadOnly = true;
            this.Descripcion_articulo.Width = 180;
            // 
            // unidad
            // 
            this.unidad.DataPropertyName = "unidad";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.unidad.DefaultCellStyle = dataGridViewCellStyle1;
            this.unidad.HeaderText = "Unidad";
            this.unidad.Name = "unidad";
            this.unidad.ReadOnly = true;
            this.unidad.Width = 50;
            // 
            // modelo
            // 
            this.modelo.DataPropertyName = "modelo";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.modelo.DefaultCellStyle = dataGridViewCellStyle2;
            this.modelo.HeaderText = "Modelo";
            this.modelo.Name = "modelo";
            this.modelo.ReadOnly = true;
            this.modelo.Width = 60;
            // 
            // almacen
            // 
            this.almacen.DataPropertyName = "almacen";
            this.almacen.HeaderText = "almacén";
            this.almacen.Name = "almacen";
            this.almacen.Visible = false;
            this.almacen.Width = 60;
            // 
            // almacenuno
            // 
            this.almacenuno.DataPropertyName = "almacenuno";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.almacenuno.DefaultCellStyle = dataGridViewCellStyle3;
            this.almacenuno.HeaderText = "120001/ 3";
            this.almacenuno.Name = "almacenuno";
            this.almacenuno.ReadOnly = true;
            this.almacenuno.Width = 70;
            // 
            // almacen2
            // 
            this.almacen2.DataPropertyName = "almacendos";
            this.almacen2.HeaderText = "200001 - 203901";
            this.almacen2.Name = "almacen2";
            this.almacen2.Width = 60;
            // 
            // costo
            // 
            this.costo.DataPropertyName = "codigoprecio";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.costo.DefaultCellStyle = dataGridViewCellStyle4;
            this.costo.HeaderText = "Costo";
            this.costo.Name = "costo";
            this.costo.ReadOnly = true;
            this.costo.Width = 80;
            // 
            // stockminimo
            // 
            this.stockminimo.DataPropertyName = "minimo";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.stockminimo.DefaultCellStyle = dataGridViewCellStyle5;
            this.stockminimo.HeaderText = "Stock Mínimo";
            this.stockminimo.Name = "stockminimo";
            this.stockminimo.ReadOnly = true;
            this.stockminimo.Width = 80;
            // 
            // stockmaximo
            // 
            this.stockmaximo.DataPropertyName = "maximo";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.stockmaximo.DefaultCellStyle = dataGridViewCellStyle6;
            this.stockmaximo.HeaderText = "stock Máximo";
            this.stockmaximo.Name = "stockmaximo";
            this.stockmaximo.ReadOnly = true;
            this.stockmaximo.Width = 80;
            // 
            // stockpedido
            // 
            this.stockpedido.DataPropertyName = "pedido";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.stockpedido.DefaultCellStyle = dataGridViewCellStyle7;
            this.stockpedido.HeaderText = "Stock Pedido";
            this.stockpedido.Name = "stockpedido";
            this.stockpedido.ReadOnly = true;
            this.stockpedido.Width = 80;
            // 
            // stockactual
            // 
            this.stockactual.DataPropertyName = "stock";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.stockactual.DefaultCellStyle = dataGridViewCellStyle8;
            this.stockactual.HeaderText = "Stock Actual";
            this.stockactual.Name = "stockactual";
            this.stockactual.ReadOnly = true;
            this.stockactual.Width = 80;
            // 
            // TipoImpuesto
            // 
            this.TipoImpuesto.DataPropertyName = "impuesto";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            dataGridViewCellStyle9.NullValue = null;
            this.TipoImpuesto.DefaultCellStyle = dataGridViewCellStyle9;
            this.TipoImpuesto.HeaderText = "Tipo Impuesto";
            this.TipoImpuesto.Name = "TipoImpuesto";
            this.TipoImpuesto.ReadOnly = true;
            this.TipoImpuesto.Width = 50;
            // 
            // nuevopedido
            // 
            this.nuevopedido.DataPropertyName = "nuevopedido";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N2";
            dataGridViewCellStyle10.NullValue = null;
            this.nuevopedido.DefaultCellStyle = dataGridViewCellStyle10;
            this.nuevopedido.HeaderText = "Pedido Sugerido";
            this.nuevopedido.Name = "nuevopedido";
            this.nuevopedido.Width = 80;
            // 
            // pedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1165, 556);
            this.Controls.Add(this.panelPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "pedido";
            this.Text = "pedido";
            this.Load += new System.EventHandler(this.pedido_Load);
            this.panelPrincipal.ResumeLayout(false);
            this.panelCentral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panelInferior.ResumeLayout(false);
            this.panelSuperior.ResumeLayout(false);
            this.panelSuperior.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelPrincipal;
        private System.Windows.Forms.Panel panelSuperior;
        private System.Windows.Forms.TextBox txtNombreProveedor;
        private System.Windows.Forms.TextBox txtCodigoProveedor;
        private System.Windows.Forms.Label lblNombreProveedor;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Panel panelCentral;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panelInferior;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.SimpleButton btnSecundaria;
        private DevExpress.XtraEditors.SimpleButton btnPrimaria;
        private System.Windows.Forms.Label lblUnidad;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Seleccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion_articulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn unidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn almacen;
        private System.Windows.Forms.DataGridViewTextBoxColumn almacenuno;
        private System.Windows.Forms.DataGridViewTextBoxColumn almacen2;
        private System.Windows.Forms.DataGridViewTextBoxColumn costo;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockminimo;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockmaximo;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockpedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockactual;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoImpuesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn nuevopedido;
    }
}