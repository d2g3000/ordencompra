﻿using System;
using System.Windows.Forms;
using Softech.Base.UI.Components.Comunes;


namespace utilitario
{
    //JI <summary>
    //!!! Clase utilitaria para agregar controles a pantallas del sistema
    //111 </summary>
    static public class Controles
    {
        //JI<summary>
        //!í.' AgregarBoton. Permite agregar botones en pantallas del sistema, en un contenedor específico y con un control existente en la pantalla
        //-'! como punto de referencia.
        //45-60
        //Desarrollo de Funcionalidad Adicional Profit Plus 2KDoce
        //EJEMPLOS PRÁCTICOS
        //111 </summary>
        //reo·" '!",...,e-- 'forma'> Objeto que contiene instancia de la pantalla <<le
        //· ' <par::> ·: ·•ane;:;; - Jr:'t- ·eContenec -· > Contenedor donde se desea agregar el control "' )2f0 'Yl>
        //111 <param name = "nuevoboton" > Botón a agregar</param>
        //"'r "'"' .., Control a usar como referencia para agregar el botón<;;:k r •. -
        //<param name = "iUb¡cac1onX · Incremento/decremento de la posición horizontal del control a agregar respecto al control referencia
        //< íparam >
        //!' <parc11n :Jame=" Ubtcac¡or y'",o Incremento/decremento de la posición vertical del control a agregar respecto al control referencia </oaram>
        //;¡¡ <param namff'"''tA!to'·.., Incremento/decremento del alto del control a agregar respecto al control referencia param>
        //<: n•m 'larne=· A"Cf'O"> Incremento/decremento del ancho del control a agregar respecto al control referencia</par2•n>
        static public void AgregarBoton(Object forma, string nombreContenedor, ref Button nuevoboton, string nombreControiReferencia, int
        iUbicacionX, int iUbicacionY, int iAito, int iAncho)
        //46-60
        {
            var ContenedorActual = new Control("NULL");
            var ControlReferenciaActual = new Control("NULL");
            ContenedorActual = string.IsNullOrEmpty(nombreContenedor) ? ((Form)forma) : BuscarControl(((Control)forma), nombreContenedor);
            ControlReferenciaActual = BuscarControl(((Control)forma), nombreControiReferencia);

            iUbicacionX += ControlReferenciaActual.Left;
            iUbicacionY += ControlReferenciaActual.Top;
            iAito += ControlReferenciaActual.Height;
            iAncho += ControlReferenciaActual.Width;

            nuevoboton.Left = iUbicacionX;
            nuevoboton.Top = iUbicacionY;
            nuevoboton.Height = iAito;
            nuevoboton.Width = iAncho;
            nuevoboton.Visible = true;

            ContenedorActual.Controls.Add(nuevoboton);

        }

        static public void AgregarLabel(Object forma, string nombreContenedor, ref Label nuevoLabel, string nombreControiReferencia, int
iUbicacionX, int iUbicacionY, int iAito, int iAncho)
        //46-60
        {
            var ContenedorActual = new Control("NULL");
            var ControlReferenciaActual = new Control("NULL");
            ContenedorActual = string.IsNullOrEmpty(nombreContenedor) ? ((Form)forma) : BuscarControl(((Control)forma), nombreContenedor);
            ControlReferenciaActual = BuscarControl(((Control)forma), nombreControiReferencia);

            iUbicacionX += ControlReferenciaActual.Left;
            iUbicacionY += ControlReferenciaActual.Top;
            iAito += ControlReferenciaActual.Height;
            iAncho += ControlReferenciaActual.Width;

            nuevoLabel.Left = iUbicacionX;
            nuevoLabel.Top = iUbicacionY;
            nuevoLabel.Height = iAito;
            nuevoLabel.Width = iAncho;
            nuevoLabel.Visible = true;

            ContenedorActual.Controls.Add(nuevoLabel);

        }
        //111 < summary >
        //í• BuscarControl.Permite ubicar un control, a partir de su nombre, dentro de un contenedor dado.
        //Desarrollo de Funcionalidad Adicional Profit Plus 2KDoce
        //111 </ summary >
        ///111 < param name = "ctrl" > Control Contenedor </ param >
        ///111 < param name = "NombreControl" > Nombre del Control a ubicar </ param >
        ///111 < returns > Control </ returns >
        /// 
        static public void AgregarTextBox(Object forma, string nombreContenedor, ref TextBox nuevoboton, string nombreControiReferencia, int
iUbicacionX, int iUbicacionY, int iAito, int iAncho)
      
        {
            var ContenedorActual = new Control("NULL");
            var ControlReferenciaActual = new Control("NULL");
            ContenedorActual = string.IsNullOrEmpty(nombreContenedor) ? ((Form)forma) : BuscarControl(((Control)forma), nombreContenedor);
            ControlReferenciaActual = BuscarControl(((Control)forma), nombreControiReferencia);

            iUbicacionX += ControlReferenciaActual.Left;
            iUbicacionY += ControlReferenciaActual.Top;
            iAito += ControlReferenciaActual.Height;
            iAncho += ControlReferenciaActual.Width;

            nuevoboton.Left = iUbicacionX;
            nuevoboton.Top = iUbicacionY;
            nuevoboton.Height = iAito;
            nuevoboton.Width = iAncho;
            nuevoboton.Visible = true;

            ContenedorActual.Controls.Add(nuevoboton);

        }
        static public Control BuscarControl(Control ctrl, String NombreControl)
                {
                NombreControl = NombreControl.Trim().ToLower();
                 if (ctrl.Name.Trim().ToLower().Equals(NombreControl))
                return ctrl;

                foreach (Control controlInterno in ctrl.Controls)
                {
                Control ctrResult = BuscarControl(controlInterno, NombreControl);
                if (ctrResult != null)
                    return ctrResult;
                }
                return null;
            }

    


                  static public void AgregarBotondev(Object forma, string nombreContenedor, ref DevExpress.XtraEditors.SimpleButton nuevoboton, string nombreControiReferencia, int
        iUbicacionX, int iUbicacionY, int iAito, int iAncho)
        //46-60
        {
            var ContenedorActual = new Control("NULL");
            var ControlReferenciaActual = new Control("NULL");
            ContenedorActual = string.IsNullOrEmpty(nombreContenedor) ? ((Form)forma) : BuscarControl(((Control)forma), nombreContenedor);
            ControlReferenciaActual = BuscarControl(((Control)forma), nombreControiReferencia);

            iUbicacionX += ControlReferenciaActual.Left;
            iUbicacionY += ControlReferenciaActual.Top;
            iAito += ControlReferenciaActual.Height;
            iAncho += ControlReferenciaActual.Width;

            nuevoboton.Left = iUbicacionX;
            nuevoboton.Top = iUbicacionY;
            nuevoboton.Height = iAito;
            nuevoboton.Width = iAncho;
            nuevoboton.Visible = true;

            ContenedorActual.Controls.Add(nuevoboton);

        }
        /*  static public BaseDataGridViewDevExpress BuscarDataGrid(Control ctrl, String NombreControl)
          {
              BaseDataGridViewDevExpress DataGridView;
              Control Control;
              NombreControl = NombreControl.Trim().ToLower();
              //if (ctrl.Name.Trim().ToLower().Equals(NombreControl))
              //{  

              foreach (BaseDataGridViewDevExpress controlInterno in ctrl.Controls)
              {
                      //DataGridView ctrResult = BuscarDataGrid(controlInterno, NombreControl);
                      //if (ctrResult != null)
                      //    return ctrResult;
                      if (controlInterno.Equals.ToString()==NombreControl)
                      {
                          DataGridView = controlInterno;
                          return DataGridView;
                      }
              }
              return null;
              //}
              //return null;
          }*/

        static public class AccesoBO
        {
            ///<summary>
            /// Obtener: Invoca el método de funcionalidad adicional "ObtenerObjetoReq".
            ///</summary>
            ///<param name="ObjGlobalFormaUIPC"></param>
            ///<returns>object. Objeto para acceso a campos/propiedades del registro activo.
            /// Una vez obtenido debe ser casteado al objeto de negocio correspondiente. Ejemplo: TrabajadorBO, DepartamentoBO.
            ///</returns>
            static public object Obtener(object ObjGlobalFormaUIPC)
            {
                var xBO = ObjGlobalFormaUIPC.GetType().GetMethod("ObtenerObjetoReq").Invoke(ObjGlobalFormaUIPC, new Object[] { });
                return xBO;
            }



        }

    }
}
