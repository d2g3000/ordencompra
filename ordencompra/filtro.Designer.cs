﻿namespace Requerimientos
{
    partial class filtro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(filtro));
            this.panelPrincipal = new System.Windows.Forms.Panel();
            this.panelCentral = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.lblNombreSubLinea = new System.Windows.Forms.Label();
            this.lblNombreLinea = new System.Windows.Forms.Label();
            this.txtSubLinea = new System.Windows.Forms.TextBox();
            this.txtLinea = new System.Windows.Forms.TextBox();
            this.lblSubLinea = new System.Windows.Forms.Label();
            this.lblLinea = new System.Windows.Forms.Label();
            this.panelInferior = new System.Windows.Forms.Panel();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.panelSuperior = new System.Windows.Forms.Panel();
            this.lblDEscripcionProveedor = new System.Windows.Forms.Label();
            this.lblCodigoProveddor = new System.Windows.Forms.Label();
            this.txtNombreProveedor = new System.Windows.Forms.TextBox();
            this.txtCodigoProveedor = new System.Windows.Forms.TextBox();
            this.panelPrincipal.SuspendLayout();
            this.panelCentral.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelInferior.SuspendLayout();
            this.panelSuperior.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelPrincipal
            // 
            this.panelPrincipal.Controls.Add(this.panelCentral);
            this.panelPrincipal.Controls.Add(this.panelInferior);
            this.panelPrincipal.Controls.Add(this.panelSuperior);
            this.panelPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPrincipal.Location = new System.Drawing.Point(0, 0);
            this.panelPrincipal.Name = "panelPrincipal";
            this.panelPrincipal.Size = new System.Drawing.Size(599, 230);
            this.panelPrincipal.TabIndex = 0;
            // 
            // panelCentral
            // 
            this.panelCentral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelCentral.Controls.Add(this.groupBox1);
            this.panelCentral.Controls.Add(this.lblNombreSubLinea);
            this.panelCentral.Controls.Add(this.lblNombreLinea);
            this.panelCentral.Controls.Add(this.txtSubLinea);
            this.panelCentral.Controls.Add(this.txtLinea);
            this.panelCentral.Controls.Add(this.lblSubLinea);
            this.panelCentral.Controls.Add(this.lblLinea);
            this.panelCentral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCentral.Location = new System.Drawing.Point(0, 74);
            this.panelCentral.Name = "panelCentral";
            this.panelCentral.Size = new System.Drawing.Size(599, 97);
            this.panelCentral.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Location = new System.Drawing.Point(13, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 86);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(21, 53);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(91, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "CMED - CNME";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(21, 20);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(89, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "ZMED - ZNME";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // lblNombreSubLinea
            // 
            this.lblNombreSubLinea.AutoSize = true;
            this.lblNombreSubLinea.Location = new System.Drawing.Point(557, 62);
            this.lblNombreSubLinea.Name = "lblNombreSubLinea";
            this.lblNombreSubLinea.Size = new System.Drawing.Size(35, 13);
            this.lblNombreSubLinea.TabIndex = 5;
            this.lblNombreSubLinea.Text = "label2";
            this.lblNombreSubLinea.Visible = false;
            // 
            // lblNombreLinea
            // 
            this.lblNombreLinea.AutoSize = true;
            this.lblNombreLinea.Location = new System.Drawing.Point(550, 12);
            this.lblNombreLinea.Name = "lblNombreLinea";
            this.lblNombreLinea.Size = new System.Drawing.Size(35, 13);
            this.lblNombreLinea.TabIndex = 4;
            this.lblNombreLinea.Text = "label1";
            this.lblNombreLinea.Visible = false;
            // 
            // txtSubLinea
            // 
            this.txtSubLinea.Location = new System.Drawing.Point(444, 62);
            this.txtSubLinea.Name = "txtSubLinea";
            this.txtSubLinea.Size = new System.Drawing.Size(100, 21);
            this.txtSubLinea.TabIndex = 3;
            this.txtSubLinea.Visible = false;
            this.txtSubLinea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSubLinea_KeyDown);
            this.txtSubLinea.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSubLinea_KeyPress);
            // 
            // txtLinea
            // 
            this.txtLinea.Location = new System.Drawing.Point(444, 4);
            this.txtLinea.Name = "txtLinea";
            this.txtLinea.Size = new System.Drawing.Size(100, 21);
            this.txtLinea.TabIndex = 2;
            this.txtLinea.Visible = false;
            this.txtLinea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLinea_KeyDown);
            this.txtLinea.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLinea_KeyPress);
            // 
            // lblSubLinea
            // 
            this.lblSubLinea.AutoSize = true;
            this.lblSubLinea.Location = new System.Drawing.Point(385, 70);
            this.lblSubLinea.Name = "lblSubLinea";
            this.lblSubLinea.Size = new System.Drawing.Size(53, 13);
            this.lblSubLinea.TabIndex = 1;
            this.lblSubLinea.Text = "Sub Linea";
            this.lblSubLinea.Visible = false;
            // 
            // lblLinea
            // 
            this.lblLinea.AutoSize = true;
            this.lblLinea.Location = new System.Drawing.Point(397, 12);
            this.lblLinea.Name = "lblLinea";
            this.lblLinea.Size = new System.Drawing.Size(32, 13);
            this.lblLinea.TabIndex = 0;
            this.lblLinea.Text = "Linea";
            this.lblLinea.Visible = false;
            // 
            // panelInferior
            // 
            this.panelInferior.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelInferior.Controls.Add(this.btnNext);
            this.panelInferior.Controls.Add(this.btnSalir);
            this.panelInferior.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelInferior.Location = new System.Drawing.Point(0, 171);
            this.panelInferior.Name = "panelInferior";
            this.panelInferior.Size = new System.Drawing.Size(599, 59);
            this.panelInferior.TabIndex = 1;
            // 
            // btnNext
            // 
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.Location = new System.Drawing.Point(339, 17);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(90, 30);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = "Siguiente";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(469, 17);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(90, 30);
            this.btnSalir.TabIndex = 0;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // panelSuperior
            // 
            this.panelSuperior.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelSuperior.Controls.Add(this.lblDEscripcionProveedor);
            this.panelSuperior.Controls.Add(this.lblCodigoProveddor);
            this.panelSuperior.Controls.Add(this.txtNombreProveedor);
            this.panelSuperior.Controls.Add(this.txtCodigoProveedor);
            this.panelSuperior.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSuperior.Location = new System.Drawing.Point(0, 0);
            this.panelSuperior.Name = "panelSuperior";
            this.panelSuperior.Size = new System.Drawing.Size(599, 74);
            this.panelSuperior.TabIndex = 0;
            // 
            // lblDEscripcionProveedor
            // 
            this.lblDEscripcionProveedor.AutoSize = true;
            this.lblDEscripcionProveedor.Location = new System.Drawing.Point(10, 40);
            this.lblDEscripcionProveedor.Name = "lblDEscripcionProveedor";
            this.lblDEscripcionProveedor.Size = new System.Drawing.Size(61, 13);
            this.lblDEscripcionProveedor.TabIndex = 3;
            this.lblDEscripcionProveedor.Text = "Descripción";
            // 
            // lblCodigoProveddor
            // 
            this.lblCodigoProveddor.AutoSize = true;
            this.lblCodigoProveddor.Location = new System.Drawing.Point(10, 10);
            this.lblCodigoProveddor.Name = "lblCodigoProveddor";
            this.lblCodigoProveddor.Size = new System.Drawing.Size(47, 13);
            this.lblCodigoProveddor.TabIndex = 2;
            this.lblCodigoProveddor.Text = "Código: ";
            // 
            // txtNombreProveedor
            // 
            this.txtNombreProveedor.Location = new System.Drawing.Point(77, 40);
            this.txtNombreProveedor.Name = "txtNombreProveedor";
            this.txtNombreProveedor.ReadOnly = true;
            this.txtNombreProveedor.Size = new System.Drawing.Size(197, 21);
            this.txtNombreProveedor.TabIndex = 1;
            // 
            // txtCodigoProveedor
            // 
            this.txtCodigoProveedor.Location = new System.Drawing.Point(80, 10);
            this.txtCodigoProveedor.Name = "txtCodigoProveedor";
            this.txtCodigoProveedor.ReadOnly = true;
            this.txtCodigoProveedor.Size = new System.Drawing.Size(100, 21);
            this.txtCodigoProveedor.TabIndex = 0;
            // 
            // filtro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 230);
            this.Controls.Add(this.panelPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "filtro";
            this.Text = "filtro";
            this.panelPrincipal.ResumeLayout(false);
            this.panelCentral.ResumeLayout(false);
            this.panelCentral.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelInferior.ResumeLayout(false);
            this.panelSuperior.ResumeLayout(false);
            this.panelSuperior.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelPrincipal;
        private System.Windows.Forms.Panel panelCentral;
        private System.Windows.Forms.Panel panelInferior;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private System.Windows.Forms.Panel panelSuperior;
        private System.Windows.Forms.TextBox txtNombreProveedor;
        private System.Windows.Forms.TextBox txtCodigoProveedor;
        private DevExpress.XtraEditors.SimpleButton btnNext;
        private System.Windows.Forms.Label lblDEscripcionProveedor;
        private System.Windows.Forms.Label lblCodigoProveddor;
        private System.Windows.Forms.TextBox txtSubLinea;
        private System.Windows.Forms.TextBox txtLinea;
        private System.Windows.Forms.Label lblSubLinea;
        private System.Windows.Forms.Label lblLinea;
        private System.Windows.Forms.Label lblNombreSubLinea;
        private System.Windows.Forms.Label lblNombreLinea;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
    }
}