﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Softech.Administrativo.BusinessObjects;
using System.Windows.Forms;
using Softech.Administrativo.DefinicionBO;
using System.Drawing;
using Softech.Base.UI.Forms;
using Softech.Base.UI.Components.Comunes;
using Softech.Administrativo.Objeto.Usuario;
using Softech.Base.DefinicionBO;
using Softech.Base.ServiceInterfaceWCF;
using Softech.Base.DefinicionError;
using System.Data.SqlClient;
using System.Data;
using Requerimientos;
using Softech.Base.BusinessObjects;
using System.Reflection;
using utilitario;
using System.Timers;
using System.Xml.Linq;
using System.IO;
using DevExpress.UserSkins;
using DevExpress.XtraEditors;
using DevExpress.Skins;


namespace Requerimientos
{
    public class ordencompra
    {
        Object ObjFormaUIPC = new object();
        Object ObjForma = new object();
        Object ObjGlobalFormaUIPC = new object();
        Object ObjGlobalForma = new object();
        Control ControlActual = new Control("NULL");
        public static ordencompra F1;
        public DevExpress.XtraEditors.SimpleButton btnAdicinal = new DevExpress.XtraEditors.SimpleButton();
        public string nombreProveedor = "";
        public string codigoProveedor = "";
        public bool AntesDeIniciarPantalla(object forma, object formaUIPC)
        {

            ordencompra.F1 = this;
            ObjGlobalFormaUIPC = formaUIPC;
            ObjGlobalForma = forma;
           // MessageBox.Show("hola");
            BonusSkins.Register();
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.Skins.SkinManager.EnableMdiFormSkins();
            //Register!
            //** 18/09/2020 **// 
            OfficeSkins.Register();
            DevExpress.Skins.SkinManager.Default.RegisterAssembly(typeof(OfficeSkins).Assembly);
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("Blue");
            //ubicacion del boton
     
            btnAdicinal.Name = "btnAdicinal";
         
            btnAdicinal.TabIndex = 25;
            btnAdicinal.TabStop = false;
            btnAdicinal.Text = "Agregar";
            btnAdicinal.Refresh();
            btnAdicinal.Cursor = System.Windows.Forms.Cursors.Hand;
           // btnAdicinal.Font = new Font("Times New Roman", 10.0f);
            btnAdicinal.Click += new EventHandler(boton_click3);

             utilitario.Controles.AgregarBotondev(ObjGlobalForma, "gbxEncabezado", ref btnAdicinal, "btnConversion",-150, -10, 0, 0);



             return true;

         }
        private void boton_click3(object sender, EventArgs e)
        {
            IOrdenCompraBO ObjNotaRecepcion = (IOrdenCompraBO)ObjGlobalFormaUIPC.GetType().GetMethod("ObtenerObjetoReq").Invoke(ObjGlobalFormaUIPC, new Object[] { });
            if (ObjNotaRecepcion.TipoObjeto != Softech.Base.DefinicionBO.AuxiliarDefinicionBOBase.TipoObjeto.Nuevo)
            {
              MessageBox.Show(" Por favor Debe seleccionar una orden de compra nueva " + "", "Nikko Apps", MessageBoxButtons.OK, MessageBoxIcon.Information);
               return;
            }
            else
            {
                if (String.IsNullOrEmpty(ObjNotaRecepcion.Co_Prov))
                {

                   MessageBox.Show("Debe seleccionar un proveedor", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                   return;
                }
                else
                { 
                // MessageBox.Show("el objeto no es nuevo");
                codigoProveedor = ObjNotaRecepcion.Co_Prov;
                StringBuilder consulta = new StringBuilder();
                consulta.Append(" select prov_des from saproveedor where co_prov = @co_prov ");
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = consulta.ToString();
                cmd.Parameters.AddWithValue("@co_prov", codigoProveedor.Trim());
                bool IsMaster = false;
                DataTable dt = ((DataSet)ObjGlobalFormaUIPC.GetType().GetMethod("EjecutarComandoSql").Invoke(ObjGlobalFormaUIPC, new object[] { ((object)cmd), ((object)IsMaster) })).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    nombreProveedor = dt.Rows[0]["prov_des"].ToString().Trim();
                }
                //MessageBox.Show(string.Format("codigo del cliente: {0} nombre del cliente: {1}", codigoProveedor, nombreProveedor));

                bool activo = false;

                for (int frmactivo = 0; frmactivo < Application.OpenForms.Count; frmactivo++)
                {
                    if (Application.OpenForms[frmactivo].Name.ToUpper() == "cambio")
                    {
                        activo = true;

                        if (Application.OpenForms[frmactivo].WindowState == FormWindowState.Minimized)
                        {
                            Application.OpenForms[frmactivo].WindowState = FormWindowState.Normal;
                        }
                        if (!Application.OpenForms[frmactivo].Focus())
                        {
                            Application.OpenForms[frmactivo].Focus();
                        }
                        break;
                    }
                    else
                    {
                        activo = false;
                    }
                }

                Form frmtextCodigo = (Form)this.ObjGlobalForma;

                filtro frm = new filtro(ObjGlobalForma, ObjGlobalFormaUIPC, codigoProveedor, nombreProveedor);
                //frm.MdiParent = frmtextCodigo;
                frm.StartPosition = FormStartPosition.CenterScreen;

                if (!activo)
                {
                    //frm.Show();
                    frm.ShowDialog();
                }
                }
            }

          
                //codigo proveedor

            }

     }
 }
