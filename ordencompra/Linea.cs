﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Softech.Administrativo.DefinicionBO;
using Softech.Administrativo.BusinessObjects;
using DevExpress.UserSkins;
using DevExpress.XtraEditors;
using DevExpress.Skins;
using System.IO;

namespace Requerimientos
{
    public partial class Linea : XtraForm
    {
        public static Linea f1;
        public Object ObjFormaUIPC = new Object();
        public Object ObjForma = new Object();
        public Object ObjGlobalFormaUIPC = new object();
        public Object ObjGlobalForma = new object();
        public DataTable dt = new DataTable();
        
        public Linea(Object Forma, Object FormaIUPC)
        {
            InitializeComponent();
            ObjGlobalForma = Forma;
            ObjGlobalFormaUIPC = FormaIUPC;
            Linea.f1 = this;
            BonusSkins.Register();
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.Skins.SkinManager.EnableMdiFormSkins();
            //Register!
            //** 18/09/2020 **// 
            OfficeSkins.Register();
            DevExpress.Skins.SkinManager.Default.RegisterAssembly(typeof(OfficeSkins).Assembly);
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("Blue");
        }

        private void Linea_Load(object sender, EventArgs e)
        {
            dt.Clear();
            StringBuilder consulta = new StringBuilder();
            consulta.Append("  select co_lin as codigo,lin_des as descripcion from saLineaArticulo  ");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = consulta.ToString();
           // cmd.Parameters.AddWithValue("@co_prov", 0);
            bool IsMaster = false;
            DataTable dtLlenarGrid = ((DataSet)ObjGlobalFormaUIPC.GetType().GetMethod("EjecutarComandoSql").Invoke(ObjGlobalFormaUIPC, new object[] { ((object)cmd), ((object)IsMaster) })).Tables[0];
            if (dtLlenarGrid.Rows.Count > 0)
            {
                dt = dtLlenarGrid;

            }

            dataGridView1.DataSource = dt;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarLinea(txtBuscar.Text.Trim());

        }

        private void BuscarLinea(string buscar)
        {
            dt.Clear();
            if (string.IsNullOrEmpty(buscar))
            {
                StringBuilder consulta2 = new StringBuilder();
                consulta2.Append("  select co_lin as codigo,lin_des as descripcion from saLineaArticulo ");
                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = consulta2.ToString();
                cmd2.Parameters.AddWithValue("@co_lin", buscar);
                bool IsMaster2 = false;
                DataTable dtLlenarGrid2 = ((DataSet)ObjGlobalFormaUIPC.GetType().GetMethod("EjecutarComandoSql").Invoke(ObjGlobalFormaUIPC, new object[] { ((object)cmd2), ((object)IsMaster2) })).Tables[0];
                if (dtLlenarGrid2.Rows.Count > 0)
                {
                    dt = dtLlenarGrid2;

                }

                dataGridView1.DataSource = dt;
                return;
            }
            StringBuilder consulta = new StringBuilder();
            consulta.Append("  select co_lin as codigo,lin_des as descripcion from saLineaArticulo where lin_des like '%" + buscar + "%' ");// @co_lin ");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = consulta.ToString();
           // cmd.Parameters.AddWithValue("@co_lin", buscar);
            bool IsMaster = false;
            DataTable dtLlenarGrid = ((DataSet)ObjGlobalFormaUIPC.GetType().GetMethod("EjecutarComandoSql").Invoke(ObjGlobalFormaUIPC, new object[] { ((object)cmd), ((object)IsMaster) })).Tables[0];
            if (dtLlenarGrid.Rows.Count > 0)
            {
                dt = dtLlenarGrid;

            }

            dataGridView1.DataSource = dt;
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                BuscarLinea(txtBuscar.Text.Trim());
               
            }

        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            filtro.f1.codigoLinea=dataGridView1.Rows[e.RowIndex].Cells["codigo"].Value.ToString().Trim();
            filtro.f1.nombreLinea= dataGridView1.Rows[e.RowIndex].Cells["descripcion"].Value.ToString().Trim();
            this.Close();
        }
    }
}
