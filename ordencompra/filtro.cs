﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Softech.Administrativo.DefinicionBO;
using Softech.Administrativo.BusinessObjects;
using DevExpress.UserSkins;
using DevExpress.XtraEditors;
using DevExpress.Skins;
using System.IO;

namespace Requerimientos
{
    public partial class filtro : XtraForm
    {
        public static filtro f1;
        public Object ObjFormaUIPC = new Object();
        public Object ObjForma = new Object();
        public Object ObjGlobalFormaUIPC = new object();
        public Object ObjGlobalForma = new object();
        public DataTable dt = new DataTable();
        private string codigoProv = "";
        public List<lineaParaBuscar> collection = new List<lineaParaBuscar>();
        //public string codigoLinea = "";
        //public string codigoSubLinea = "";
        public string codigoLinea
        {
            get { return this.txtLinea.Text; }
            set
            {
                if (value == null)
                {
                    this.txtLinea.Text = "";
                }
                else
                {
                    this.txtLinea.Text = value;
                }
            }
        }
        public string codigoSubLinea
        {
            get { return this.txtSubLinea.Text; }
            set
            {
                if (value == null)
                {
                    this.txtSubLinea.Text = "";
                }
                else
                {
                    this.txtSubLinea.Text = value;
                }
            }
        }
        public string nombreLinea
        {
            get { return this.lblNombreLinea.Text; }
            set
            {
                if (value == null)
                {
                    this.lblNombreLinea.Text = "";
                }
                else
                {
                    this.lblNombreLinea.Text = value;
                }
            }
        }
        public string NombreSubLinea
        {
            get { return this.lblNombreSubLinea.Text; }
            set
            {
                if (value == null)
                {
                    this.lblNombreSubLinea.Text = "";
                }
                else
                {
                    this.lblNombreSubLinea.Text = value;
                }
            }
        }
        public filtro(Object Forma, Object FormaIUPC, string codigoProveedor, string nombreProveedor)
        {
            InitializeComponent();
            ObjGlobalForma = Forma;
            ObjGlobalFormaUIPC = FormaIUPC;
            filtro.f1 = this;
            BonusSkins.Register();
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.Skins.SkinManager.EnableMdiFormSkins();
            //Register!
            //** 18/09/2020 **// 
            OfficeSkins.Register();
            DevExpress.Skins.SkinManager.Default.RegisterAssembly(typeof(OfficeSkins).Assembly);
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("Blue");
            txtCodigoProveedor.Text = codigoProveedor;
            txtNombreProveedor.Text = nombreProveedor;
            codigoProv = codigoProveedor;
            lblNombreLinea.Text = "";
            lblNombreSubLinea.Text = "";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            /*  if (lblNombreLinea.Text=="")
              {
                  MessageBox.Show(" Debe seleccionar una  linea ", "Nikko Apps", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                  return;
              }*/
            /*  if (lblNombreSubLinea.Text == "")
              {
                  MessageBox.Show(" Debe seleccionar una  sub linea ", "Nikko Apps", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                  return;
              }*/
            try
            {
                foreach (RadioButton radio in groupBox1.Controls)
                {
                    //si el radiobutton está checkeado
                    if (radio.Checked)
                    {
                        //muestro el nombre del radiobutton checkeado
                        if (radio.Name== "radioButton1")
                        {
                            collection.Add(new lineaParaBuscar() {lineaBuscar = "ZMED" });
                            collection.Add(new lineaParaBuscar() {lineaBuscar = "ZNME" });
                            
                        }
                        if (radio.Name == "radioButton2")
                        {
                            collection.Add(new lineaParaBuscar() { lineaBuscar = "CMED" });
                            collection.Add(new lineaParaBuscar() { lineaBuscar = "CNME" });

                        }
                    }
                }

            }
            catch (Exception exe)
            {

                MessageBox.Show("error "+exe.Message.ToString());
            }
            /* if (radioButton2.Checked==true)
             {

             }*/
     
            bool activo = false;

            for (int frmactivo = 0; frmactivo < Application.OpenForms.Count; frmactivo++)
            {
                if (Application.OpenForms[frmactivo].Name.ToUpper() == "cambio")
                {
                    activo = true;

                    if (Application.OpenForms[frmactivo].WindowState == FormWindowState.Minimized)
                    {
                        Application.OpenForms[frmactivo].WindowState = FormWindowState.Normal;
                    }
                    if (!Application.OpenForms[frmactivo].Focus())
                    {
                        Application.OpenForms[frmactivo].Focus();
                    }
                    break;
                }
                else
                {
                    activo = false;
                }
            }
            
            Form frmtextCodigo = (Form)this.ObjGlobalForma;
           
            pedido frm = new pedido(ObjGlobalForma, ObjGlobalFormaUIPC, txtCodigoProveedor.Text, txtNombreProveedor.Text);//,txtLinea.Text,txtSubLinea.Text);
            //frm.MdiParent = frmtextCodigo;
            frm.StartPosition = FormStartPosition.CenterScreen;

            if (!activo)
            {
                //frm.Show();
                frm.ShowDialog();
            }
            this.Close();
        }

        private void txtLinea_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                PantallaLinea();
            }
        }

        private void txtSubLinea_KeyDown(object sender, KeyEventArgs e)
        {
            if (lblNombreLinea.Text=="")
            {
                MessageBox.Show(" Debe seleccionar una  linea ", "Nikko Apps", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (e.KeyCode == Keys.F2)
            {
                pantallaSubLinea();
            }
        }

        private void txtLinea_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                    StringBuilder consulta = new StringBuilder();
                    consulta.Append("  select co_lin as codigo,lin_des as descripcion from saLineaArticulo where co_lin = @co_lin ");
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = consulta.ToString();
                    cmd.Parameters.AddWithValue("@co_lin", txtLinea.Text.Trim());
                    bool IsMaster = false;
                    DataTable dtLlenarGrid = ((DataSet)ObjGlobalFormaUIPC.GetType().GetMethod("EjecutarComandoSql").Invoke(ObjGlobalFormaUIPC, new object[] { ((object)cmd), ((object)IsMaster) })).Tables[0];
                if (dtLlenarGrid.Rows.Count > 0)
                {
                    txtLinea.Text = (string)dtLlenarGrid.Rows[0]["codigo"];
                    lblNombreLinea.Text = (string)dtLlenarGrid.Rows[0]["descripcion"];
                    txtSubLinea.Focus();

                }
                else
                {
                    PantallaLinea();
                }
                
            }
        }

        private void txtSubLinea_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
           

                if ((int)e.KeyChar == (int)Keys.Enter)
                {

                    if (lblNombreLinea.Text == "")
                    {
                        MessageBox.Show(" Debe seleccionar una  linea ", "Nikko Apps", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    StringBuilder consulta = new StringBuilder();
                    consulta.Append("  select co_subl as codigo,subl_des as descripcion from saSubLinea where co_subl = @co_subl and co_lin = @co_lin ");
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = consulta.ToString();
                    cmd.Parameters.AddWithValue("@co_subl", txtSubLinea.Text.Trim());
                    cmd.Parameters.AddWithValue("@co_lin", txtLinea.Text.Trim());
                    bool IsMaster = false;
                    DataTable dtLlenarGrid = ((DataSet)ObjGlobalFormaUIPC.GetType().GetMethod("EjecutarComandoSql").Invoke(ObjGlobalFormaUIPC, new object[] { ((object)cmd), ((object)IsMaster) })).Tables[0];
                if (dtLlenarGrid.Rows.Count > 0)
                {
                    txtSubLinea.Text = (string)dtLlenarGrid.Rows[0]["codigo"];
                    lblNombreSubLinea.Text = (string)dtLlenarGrid.Rows[0]["descripcion"];
                        btnNext.Focus();
                }
                else
                {
                    pantallaSubLinea();
                }

            }
            }
            catch (Exception exe)
            {

                MessageBox.Show(string.Format("error {0}",exe.Message.ToString()));
            }
        }
        private void PantallaLinea()
        {
            bool activo = false;

            for (int frmactivo = 0; frmactivo < Application.OpenForms.Count; frmactivo++)
            {
                if (Application.OpenForms[frmactivo].Name.ToUpper() == "cambio")
                {
                    activo = true;

                    if (Application.OpenForms[frmactivo].WindowState == FormWindowState.Minimized)
                    {
                        Application.OpenForms[frmactivo].WindowState = FormWindowState.Normal;
                    }
                    if (!Application.OpenForms[frmactivo].Focus())
                    {
                        Application.OpenForms[frmactivo].Focus();
                    }
                    break;
                }
                else
                {
                    activo = false;
                }
            }

            Form frmtextCodigo = (Form)this.ObjGlobalForma;

            Linea frm = new Linea(ObjGlobalForma, ObjGlobalFormaUIPC);
            //frm.MdiParent = frmtextCodigo;
            frm.StartPosition = FormStartPosition.CenterScreen;

            if (!activo)
            {
                //frm.Show();
                frm.ShowDialog();
            }
            txtSubLinea.Focus();
            return;
        }
        private void pantallaSubLinea()
        {
            bool activo = false;

            for (int frmactivo = 0; frmactivo < Application.OpenForms.Count; frmactivo++)
            {
                if (Application.OpenForms[frmactivo].Name.ToUpper() == "cambio")
                {
                    activo = true;

                    if (Application.OpenForms[frmactivo].WindowState == FormWindowState.Minimized)
                    {
                        Application.OpenForms[frmactivo].WindowState = FormWindowState.Normal;
                    }
                    if (!Application.OpenForms[frmactivo].Focus())
                    {
                        Application.OpenForms[frmactivo].Focus();
                    }
                    break;
                }
                else
                {
                    activo = false;
                }
            }

            Form frmtextCodigo = (Form)this.ObjGlobalForma;

            sublinea frm = new sublinea(ObjGlobalForma, ObjGlobalFormaUIPC,txtLinea.Text);
            //frm.MdiParent = frmtextCodigo;
            frm.StartPosition = FormStartPosition.CenterScreen;

            if (!activo)
            {
                //frm.Show();
                frm.ShowDialog();
            }
            btnNext.Focus();
            return;
        }
    }
}
